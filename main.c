#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wayland/wayland-client.h>
#include <wayland/wayland-client-protocol.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "config.h"

#define RED "\033[1;31m"
#define RESET "\033[0m"

GLFWwindow* window;
VkInstance instance;
VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

const uint32_t pixel_value = 0x0; // black

bool init_window();
bool init_vulkan();
bool dev_suitable(VkPhysicalDevice);
bool pick_physical_dev();
bool create_instance();
void create_surface();
void create_logical_dev();
void cleanup();

void yawt_log(const char*);

int main(int argc, char* argv[])
{
	if (!init_window()) {
		printf(RED "init_window FAILED\n" RESET);
	}
	uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(NULL, &extensionCount, NULL);

    printf("%d extensions supported\n", extensionCount);

    while( !glfwWindowShouldClose( window ) )
    {
        glfwPollEvents( );
    }

    glfwDestroyWindow( window );

    glfwTerminate( );

    return 0;
	
/*
	if (!init_vulkan()) {
		printf(RED "init_vulkan FAILED\n" RESET);
		goto fail;
	}

fail:
	cleanup();
	return EXIT_SUCCESS;
*/
}

bool
init_window()
{
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	//glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	window = glfwCreateWindow(800, 600, "YAWT", NULL, NULL);
	if (window == NULL)
		return false;
	return true;
}

bool
init_vulkan()
{
	bool res = create_instance();
	if (!res) {
		printf(RED "create_instance FAILED\n" RESET);
		goto fail;
	}
	res = pick_physical_dev();
	if (!res) {
		printf(RED "pick_physical_dev FAILED\n" RESET);
		goto fail;
	}
fail:
	return res;
}

bool
dev_suitable(VkPhysicalDevice dev)
{
	VkPhysicalDeviceProperties dev_props;
	VkPhysicalDeviceFeatures dev_feats;
	vkGetPhysicalDeviceProperties(dev, &dev_props);
	vkGetPhysicalDeviceFeatures(dev, &dev_feats);

	// FIXME: Find better check
	//return dev_props.deviceType != VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU;
	return dev_props.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU;
}

bool
pick_physical_dev()
{
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);

	if (deviceCount == 0) {
		printf(RED "failed to find GPUs with Vulkan support!\n" RESET);
		return false;
	}
	else if (deviceCount > 2) {
		deviceCount = 2;
	}

	VkPhysicalDevice devs[2] = {0};
	vkEnumeratePhysicalDevices(instance, &deviceCount, devs);

	for (int i = 0; i < deviceCount; i++) {
		if (dev_suitable(devs[i])) {
			physicalDevice = devs[i];
			break;
		}
	}
	if (physicalDevice == VK_NULL_HANDLE) {
		printf(RED "Failed to find suitable vulkan GPU\n" RESET);
		return false;
	}
	return true;
}

bool
create_instance()
{
	VkApplicationInfo appInfo = { 0 };
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "YAWT";
	appInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
	appInfo.apiVersion = VK_API_VERSION_1_2;

	VkInstanceCreateInfo createInfo = { 0 };
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	uint32_t glfwExtCnt = 0;
	const char** glfwExts;

	glfwExts = glfwGetRequiredInstanceExtensions(&glfwExtCnt);
	//printf("%d\n", glfwExtCnt);

	createInfo.enabledExtensionCount = glfwExtCnt;
	createInfo.ppEnabledExtensionNames = glfwExts;
	createInfo.enabledLayerCount = 0;

	VkResult result = vkCreateInstance(&createInfo, NULL, &instance);
	if (result != VK_SUCCESS) {
		return false;
	}
	return true;
}

void
cleanup()
{
	// Instance is the last Vulkan resource to be destroyed
	vkDestroyInstance(instance, NULL);
	glfwDestroyWindow(window);
	glfwTerminate();
}

void
yawt_log(const char* msg)
{
#ifdef DEBUG
	fprintf(stderr, msg);
	fprintf(stderr, "\n");
#endif
}
