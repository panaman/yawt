# yawt (Yet Another Wayland Terminal)

## Experiment for creating a terminal for Linux. 

- Purpose: Learning C and wayland
- Inspired by suckless 'st' terminal built for X11
- Meant for window-manager based compositors (eg. Sway, bspwc)

